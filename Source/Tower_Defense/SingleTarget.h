// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "SingleTarget.generated.h"

/**
 * 
 */
UCLASS()
class TOWER_DEFENSE_API ASingleTarget : public ATower
{
	GENERATED_BODY()
	
public:

	ASingleTarget();

protected:

		void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

		void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			float AttackSpeed;

private: 

	UPROPERTY(EditAnywhere, Category = "Config")
		TSubclassOf<class AProjectile> BP_Projectile;

	UFUNCTION()
		void RotateBarrel();

	UFUNCTION()
		void SetTarget();

	UFUNCTION()
		void Fire();

	FTimerHandle TimerDelayHandle;

	void Tick(float DeltaTime) override;


};
