// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BurnEffect.generated.h"

UCLASS()
class TOWER_DEFENSE_API ABurnEffect : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABurnEffect();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class AEnemyCharacter* Enemy;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Duration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 DamagePerTick;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BurnTimer;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent)
		void Activate(AEnemyCharacter* target);

};
