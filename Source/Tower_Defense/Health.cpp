// Fill out your copyright notice in the Description page of Project Settings.


#include "Health.h"

// Sets default values for this component's properties
UHealth::UHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	DefaultHealth = 100;
	Health = DefaultHealth;
	// ...
}


// Called when the game starts
void UHealth::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UHealth::TakeDamage(int32 damage)
{
	Health -= damage;

	if (Health <= 0)
	{
		EnemyDeath.Broadcast(this);
	}

	//UE_LOG(LogTemp, Warning, TEXT("Took dmg"));
}

void UHealth::ScaleHealth(float difficulty)
{
	Health *= difficulty;
}

