// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCore.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/BoxComponent.h"
#include "EnemyCharacter.h"

// Sets default values
APlayerCore::APlayerCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PlayerCore = CreateDefaultSubobject<USceneComponent>(TEXT("PlayerCore"));
	SetRootComponent(PlayerCore);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("ExitTrigger"));
	ExitTrigger->SetupAttachment(PlayerCore);
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &APlayerCore::OnOverlapBegin);
}

// Called when the game starts or when spawned
void APlayerCore::BeginPlay()
{
	Super::BeginPlay();
	
}

void APlayerCore::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(OtherActor))
	{
		Exited.Broadcast(Enemy);
		UE_LOG(LogTemp, Warning, TEXT("Enemy passed"));
	}
}

// Called every frame
void APlayerCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

