// Fill out your copyright notice in the Description page of Project Settings.


#include "BankSystem.h"
#include "Kismet/GameplayStatics.h"
#include "Salvager.h"

// Sets default values
ABankSystem::ABankSystem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Gold = 1000;
}

// Called when the game starts or when spawned
void ABankSystem::BeginPlay()
{
	Super::BeginPlay();
	
}

bool ABankSystem::CanBuy(int32 price)
{
	if (Gold >= price)
	{
		Gold -= price;
		return true;
	}
	else
	{
		return false;
	}
}

// Called every frame
void ABankSystem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

