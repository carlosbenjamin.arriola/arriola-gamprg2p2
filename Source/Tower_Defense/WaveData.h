// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

/**
 * 
 */
UCLASS(BlueprintType)
class TOWER_DEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()

public: 

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Duration;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 NumSpawns;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<class AEnemyCharacter>> Monsters;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class AEnemyCharacter> Boss;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reward")
	int32 KillReward;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Reward")
	int32 ClearReward;

	UFUNCTION(BlueprintPure, BlueprintCallable)
	int32 TotalKillReward();


};
