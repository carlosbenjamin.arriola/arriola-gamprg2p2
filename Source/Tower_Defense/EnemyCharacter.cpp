// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyCharacter.h"
#include "MyAIController.h"
#include "Waypoint.h"
#include "Health.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AEnemyCharacter::AEnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CurrentWaypoint = 0;
	Damage = 10;
	
	HealthComponent = CreateDefaultSubobject<UHealth>(TEXT("HealthComponent"));
	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
}

// Called when the game starts or when spawned
void AEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();

	HealthComponent->EnemyDeath.AddDynamic(this, &AEnemyCharacter::Die);
}

void AEnemyCharacter::MoveToWaypoint()
{
	AMyAIController* MyAIController = Cast<AMyAIController>(GetController());

	if (MyAIController)
	{
		if (CurrentWaypoint < waypoints.Num())
		{
			MyAIController->MoveToActor(waypoints[CurrentWaypoint]);
			CurrentWaypoint++;
		}
	}
}

int32 AEnemyCharacter::GetDamage()
{
	return Damage;
}


void AEnemyCharacter::Die(UHealth* health)
{
	Destroy();
}

// Called every frame
void AEnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemyCharacter::InitEnemy()
{
	MoveToWaypoint();
}

