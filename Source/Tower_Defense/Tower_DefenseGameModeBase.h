// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Tower_DefenseGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TOWER_DEFENSE_API ATower_DefenseGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
