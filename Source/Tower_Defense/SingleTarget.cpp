// Fill out your copyright notice in the Description page of Project Settings.


#include "SingleTarget.h"
#include "EnemyCharacter.h"
#include "Projectile.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/ArrowComponent.h"


ASingleTarget::ASingleTarget()
{

}


void ASingleTarget::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
}

void ASingleTarget::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
}

void ASingleTarget::RotateBarrel()
{
	FRotator rot = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), target->GetActorLocation());
	SetActorRotation(rot);
}

void ASingleTarget::SetTarget()
{
	if (EnemiesInRange.Num() > 0)
	{
		target = Cast<AEnemyCharacter>(EnemiesInRange[0]);

		RotateBarrel();
		Fire();
	}
}

void ASingleTarget::Fire()
{
	//UE_LOG(LogTemp, Warning, TEXT("Firing"));
	
	if (target)
	{
		AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(BP_Projectile, ArrowComp->GetComponentTransform());
		FTimerDelegate TimerDel;

		TimerDel.BindUFunction(this, FName("Fire"));
		GetWorld()->GetTimerManager().SetTimer(TimerDelayHandle, TimerDel, AttackSpeed, false);
	}
}

void ASingleTarget::Tick(float DeltaTime)
{
	if (target == nullptr)
	{
		SetTarget();
	}
	else
		RotateBarrel();
}
