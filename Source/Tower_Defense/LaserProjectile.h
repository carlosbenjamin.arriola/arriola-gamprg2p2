// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "LaserProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWER_DEFENSE_API ALaserProjectile : public AProjectile
{
	GENERATED_BODY()
	

public:

	ALaserProjectile();

protected:

	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

private:

	UPROPERTY(EditAnywhere, Category = "Config")
		TSubclassOf<class AActor> BP_Burn;
};
