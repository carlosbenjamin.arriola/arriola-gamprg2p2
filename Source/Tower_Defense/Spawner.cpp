// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "EnemyCharacter.h"
#include "WaveData.h"
#include "Health.h"


// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CurrentNum = 0;
}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

class AEnemyCharacter* ASpawner::SpawnEnemy(TSubclassOf<class AEnemyCharacter> enemy)
{
	FActorSpawnParameters SpawnParam;

	FTransform transform = FTransform(FRotator(0), GetActorLocation());

	AEnemyCharacter* Monster = GetWorld()->SpawnActor<AEnemyCharacter>(enemy, GetActorLocation(), FRotator(0), SpawnParam); 
	//Init enemy (set waypoints)
	Monster->waypoints = waypoints;
	Monster->HealthComponent->ScaleHealth(1.1f);
	Monster->InitEnemy();

	return Monster;
}
