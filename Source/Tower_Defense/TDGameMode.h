// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TOWER_DEFENSE_API ATDGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	ATDGameMode();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class UWaveData*> WaveData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ASpawner*> Spawners;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 currentSpawned;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 maxSpawned;

	

	virtual void BeginPlay() override;

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 WaveCounter;

	UFUNCTION(BlueprintCallable)
		void StartWave();

	UFUNCTION()
		void EndWave();

	UFUNCTION()
		void GoldUponKill(class UHealth* health);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ABankSystem* bank;	
};
