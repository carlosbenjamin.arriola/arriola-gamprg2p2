// Fill out your copyright notice in the Description page of Project Settings.


#include "MyAIController.h"
#include "EnemyCharacter.h"


void AMyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	Super::OnMoveCompleted(RequestID, Result);

	AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(GetPawn());

	Enemy->MoveToWaypoint();
}
