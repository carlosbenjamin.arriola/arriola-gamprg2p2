// Fill out your copyright notice in the Description page of Project Settings.


#include "MissileProjectile.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "EnemyCharacter.h"
#include "Health.h"


AMissileProjectile::AMissileProjectile()
{

}


void AMissileProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if (AEnemyCharacter* enemy = Cast<AEnemyCharacter>(OtherActor))
	{
		FVector SphereSpawnLocation = enemy->GetActorLocation();
		TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
		traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
		TArray<AActor*> enemies;
		TArray<AActor*> ignoreActors;
		UClass* seekClass = AEnemyCharacter::StaticClass();
		UKismetSystemLibrary::SphereOverlapActors(GetWorld(), SphereSpawnLocation, 4000.0f, traceObjectTypes, seekClass, ignoreActors, enemies);

		for (AActor* actor : enemies)
		{
			if (AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(actor))
			{
				Enemy->HealthComponent->TakeDamage(damage);
				UE_LOG(LogTemp, Warning, TEXT("Enemies"));
			}
		}
	}

}
