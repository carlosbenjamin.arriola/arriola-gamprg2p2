// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "Salvager.generated.h"

/**
 * 
 */

UCLASS()
class TOWER_DEFENSE_API ASalvager : public ATower
{
	GENERATED_BODY()

public:

	ASalvager();

protected:


	virtual void BeginPlay() override;

	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;


	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 bonusGold = 3;

	UFUNCTION()
		void GainBonusGold(UHealth* health);

public:


private:

};
