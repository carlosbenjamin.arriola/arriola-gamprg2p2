// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

UCLASS()
class TOWER_DEFENSE_API ATower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* TowerSceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* TowerStaticMeshBase;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* TowerStaticMeshTurret;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UArrowComponent* ArrowComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USphereComponent* TowerRange;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Enemies in Range")
		TArray<AActor*> EnemiesInRange;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Current Target")
		AActor* target;
	
	UFUNCTION()
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void RemoveEnemyOnDeath(class UHealth* health);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;



};
