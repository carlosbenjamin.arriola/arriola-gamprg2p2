// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SlowEffect.generated.h"

UCLASS()
class TOWER_DEFENSE_API ASlowEffect : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASlowEffect();

protected:
	// Called when the game starts or when spawned
	// Slow variables and timer to be used in BP

	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent)
		void Activate(AEnemyCharacter* target);

};
