// Fill out your copyright notice in the Description page of Project Settings.


#include "Salvager.h"
#include "Health.h"
#include "EnemyCharacter.h"
#include "BankSystem.h"

ASalvager::ASalvager()
{

}

void ASalvager::BeginPlay()
{
	Super::BeginPlay();
}

void ASalvager::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Super::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	
}


void ASalvager::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
}

void ASalvager::GainBonusGold(UHealth* health)
{

}
