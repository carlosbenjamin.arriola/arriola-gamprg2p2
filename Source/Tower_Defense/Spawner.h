// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Spawner.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWaveEnded);

UCLASS()
class TOWER_DEFENSE_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AActor*> waypoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 WaveCount;

	FTimerHandle TimerDelayHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UWaveData* data;


	class AEnemyCharacter* SpawnEnemy(TSubclassOf<class AEnemyCharacter> Enemies);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 CurrentNum;

	UPROPERTY(BlueprintAssignable)
		FOnWaveEnded WaveEnded;
};
