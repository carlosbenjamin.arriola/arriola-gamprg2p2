// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/SphereComponent.h"
#include "Health.h"
#include "EnemyCharacter.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TowerSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("TowerSceneComp"));
	SetRootComponent(TowerSceneComponent);

	TowerStaticMeshBase = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TowerStaticMeshBase"));
	TowerStaticMeshBase->SetupAttachment(TowerSceneComponent);

	TowerStaticMeshTurret = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TowerStaticMeshTurret"));
	TowerStaticMeshTurret->SetupAttachment(TowerSceneComponent);

	ArrowComp = CreateDefaultSubobject<UArrowComponent>(TEXT("ArrowComponent"));
	ArrowComp->SetupAttachment(TowerStaticMeshTurret);

	TowerRange = CreateDefaultSubobject<USphereComponent>(TEXT("TowerRange"));
	TowerRange->SetupAttachment(TowerSceneComponent);
	TowerRange->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnOverlapBegin);
	TowerRange->OnComponentEndOverlap.AddDynamic(this, &ATower::OnOverlapEnd);
	
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATower::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(OtherActor))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Enemy overlapping"));
		EnemiesInRange.Add(Enemy);
		Enemy->HealthComponent->EnemyDeath.AddDynamic(this, &ATower::RemoveEnemyOnDeath);
	}
}

void ATower::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(OtherActor))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Enemy exited"));
		EnemiesInRange.Remove(Enemy);
		Enemy->HealthComponent->EnemyDeath.RemoveDynamic(this, &ATower::RemoveEnemyOnDeath);
	}
}

void ATower::RemoveEnemyOnDeath(UHealth* health)
{
	AEnemyCharacter* enemy = Cast<AEnemyCharacter>(health->GetOwner());
	EnemiesInRange.Remove(enemy);

	target = nullptr;
}
