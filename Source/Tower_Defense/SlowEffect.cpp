// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowEffect.h"
#include "EnemyCharacter.h"

// Sets default values
ASlowEffect::ASlowEffect()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASlowEffect::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASlowEffect::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

