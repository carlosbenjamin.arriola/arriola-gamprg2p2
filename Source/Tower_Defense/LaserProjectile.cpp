// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserProjectile.h"
#include "EnemyCharacter.h"
#include "BurnEffect.h"



ALaserProjectile::ALaserProjectile()
{

}

void ALaserProjectile::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEnemyCharacter* Enemy = Cast<AEnemyCharacter>(OtherActor))
	{
		FTransform EnemyTransform = Enemy->GetActorTransform();

		ABurnEffect* burnEffect = GetWorld()->SpawnActor<ABurnEffect>(BP_Burn, EnemyTransform);
		burnEffect->Activate(Enemy);
		Destroy();
	}
}
