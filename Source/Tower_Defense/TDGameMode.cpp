// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGameMode.h"
#include "Spawner.h"
#include "Kismet/GameplayStatics.h"
#include "BankSystem.h"
#include "WaveData.h"
#include "EnemyCharacter.h"
#include "Health.h"

ATDGameMode::ATDGameMode()
{
	WaveCounter = 0;
	currentSpawned = 0;
}

void ATDGameMode::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle timerHandle;
	FTimerDelegate del;

	del.BindLambda([this] { StartWave(); });
	GetWorldTimerManager().SetTimer(timerHandle, del, 3.0f, false);
}

void ATDGameMode::StartWave()
{
	maxSpawned = WaveData[WaveCounter]->NumSpawns;
	FTimerHandle TimerDelayHandle;

	if (currentSpawned < maxSpawned)
	{
		GetWorldTimerManager().SetTimer(TimerDelayHandle, this, &ATDGameMode::StartWave, 2.5f, false);
		AEnemyCharacter* Monster = Spawners[FMath::RandRange(0, 3)]->SpawnEnemy(WaveData[WaveCounter]->Monsters[0]);
		Monster->HealthComponent->EnemyDeath.AddDynamic(this, &ATDGameMode::GoldUponKill);
		currentSpawned++;
	}
	else
	{
		EndWave();
	}
}

void ATDGameMode::EndWave()
{
	bank->Gold += WaveData[WaveCounter]->ClearReward;

	WaveCounter++;
	currentSpawned++;

	if (currentSpawned >= maxSpawned)
	{
		currentSpawned = 0;
		StartWave();
	}
}

void ATDGameMode::GoldUponKill(UHealth* health)
{
	bank->Gold += WaveData[WaveCounter]->KillReward;
}