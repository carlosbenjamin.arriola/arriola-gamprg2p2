// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "MissileProjectile.generated.h"

/**
 * 
 */
UCLASS()
class TOWER_DEFENSE_API AMissileProjectile : public AProjectile
{
	GENERATED_BODY()

public: 

	AMissileProjectile();

protected:

		void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

private:


	
};
