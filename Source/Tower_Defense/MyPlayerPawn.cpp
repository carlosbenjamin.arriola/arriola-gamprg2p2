// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPlayerPawn.h"
#include "PlayerCore.h"
#include "EnemyCharacter.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
AMyPlayerPawn::AMyPlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PlayerHealth = 100;
	PlayerGold = 1000;
}

// Called when the game starts or when spawned
void AMyPlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APlayerCore::StaticClass(), Cores);
	for (AActor* PlayerCore : Cores)
	{
		Cast<APlayerCore>(PlayerCore)->Exited.AddDynamic(this, &AMyPlayerPawn::TakeDamage);
	}
}

// Called every frame
void AMyPlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMyPlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AMyPlayerPawn::TakeDamage(AEnemyCharacter* Enemy)
{
	AEnemyCharacter* enemy = Enemy;
	PlayerHealth -= enemy->GetDamage();
	UE_LOG(LogTemp, Warning, TEXT("Take Damage"));
	UE_LOG(LogTemp, Warning, TEXT("Player HP: %d"), PlayerHealth);
	enemy->Destroy();
}

